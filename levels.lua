
-- color functions

function randomColor()
    return rainbowColors[math.random(1,#rainbowColors)]
end

function mostGreySomeColor()
    if math.random() > 0.33 then
        return {0.5, 0.5, 0.5}
    else
        return rainbowColors[1]
    end
end

function everySecondColor(i)
    if i % 2 == 0 then
        return {0.5, 0.5, 0.5}
    else
        return rainbowColors[1]
    end
end

-- ring functions

function ring(radius, n, colorFunction, immune, immunity)
    local brickPadding = TAU/400
    local brickWidth = TAU/n - brickPadding
    for i = 1, n do
        local centerDirection = i*TAU/n - brickWidth/2 - brickPadding/2
        table.insert(bricks, RoundBrick:new(radius, centerDirection - brickWidth/2, centerDirection + brickWidth/2, colorFunction(i), immune or false, immunity or 0))
    end
end

function solidBrickRing(radius, n, colorFunction)
    local brickPadding = TAU/400
    local brickWidth = TAU/n - brickPadding
    for i = 1, n do
        local centerDirection = i*TAU/n - brickWidth/2 - brickPadding/2
        table.insert(bricks, RoundBrick:new(radius, centerDirection - brickWidth/2, centerDirection + brickWidth/2, colorFunction(i), true, 0))
    end
end

function everyOtherBrickRing(radius, n, colorFunction, immune, mod1, mod2, immunity)
    local brickPadding = TAU/400
    local brickWidth = TAU/n - brickPadding
    for i = 1, n do
        if i%(mod1 or 2) == (mod2 or 0) then
            local centerDirection = i*TAU/n - brickWidth/2 - brickPadding/2
            table.insert(bricks, RoundBrick:new(radius, centerDirection - brickWidth/2, centerDirection + brickWidth/2, colorFunction(i), immune or false, immunity or 0))
        end
    end
end

function everyOtherSolidBrickRing(radius, n, colorFunction, mod1, mod2)
    local brickPadding = TAU/400
    local brickWidth = TAU/n - brickPadding
    for i = 1, n do
        if i%(mod1 or 2) == (mod2 or 0) then
            local centerDirection = i*TAU/n - brickWidth/2 - brickPadding/2
            table.insert(bricks, RoundBrick:new(radius, centerDirection - brickWidth/2, centerDirection + brickWidth/2, colorFunction(i), true, 0))
        end
    end
end
-- levels definitions

function insideOutside()
    scene.paddle.radius = 350

    -- outer rings of bricks
    for n = 1, 3 do
        local brickLength = 360 / (20 + (3 - n) * 3)
        local brickDistance = 2.5
        local rowWidth = 40
        for i = 0, 359, brickLength do
            local color = rainbowColors[math.random(1,6)]
            table.insert(bricks, RoundBrick:new(CENTER.y - n * rowWidth, i * TAU / 360, (i + brickLength - brickDistance) * TAU / 360, color, false))
        end
    end

    -- unbreakable wall around the level
    local w = 360 / (20)
    local wa = 2.5
    for i = 0, 359, w do
        table.insert(bricks, RoundBrick:new(CENTER.y, i * TAU / 360, (i + w - wa) * TAU / 360, darkGray, true))
    end

end

function emptyLevel()
    levelSettings.radius = 100
end

function simpleLevel()
    levelSettings.radius = 100

    local s = TAU/64 -- brick separation
    table.insert(bricks, RoundBrick:new(CANVAS_HEIGHT/2 - 50, 0+s, TAU/2-s, rainbowColors[1], false))
    table.insert(bricks, RoundBrick:new(CANVAS_HEIGHT/2 - 50, TAU/2+s, TAU-s, rainbowColors[2], false))
end

function tutorialLevel()
    levelSettings.radius = 350

    -- Ring of big grey blocks, with some in a single color.
    ring(450, 10, everySecondColor)
end

function halvesLevel()
    levelSettings.radius = 330

    levelSettings.star.color = {1,0,0}
    levelSettings.brightstar.color = {1,1,0}
    levelSettings.circles.active = false
    levelSettings.circles.minRadius = 480
    levelSettings.lines.animated = false
    levelSettings.lines.color = {0,0,0.5}
    levelSettings.backgroundColor = {0.0,0.0,0.1,1}
    levelSettings.clouds = { -- every cloud definition MUST have a different color per level!
        {
            color = {0.10,0.00,0.00},
            pos = vector(CENTER.x,80)
        },
        {
            color = {0.10,0.00,0.05},
            pos = vector(CENTER.x,1000)
        }
    }

    -- Upper side is one color, lower side is second.
    local n = 10
    ring(400, n, function(i)
        if i <= n/2 then
            return rainbowColors[1]
        else
            return rainbowColors[5]
        end
    end)
    everyOtherSolidBrickRing(450,30, function()
        return darkGrey
    end, 3, 0)
    everyOtherSolidBrickRing(450,30, function()
        return darkGrey
    end, 3, 1)

    -- for _, brick in ipairs(bricks) do
    --     brick:setImmunity(2)
    -- end
end

function noRiskNoFun()
    levelSettings.radius = 300

    levelSettings.circles.minRadius = 600
    levelSettings.circles.maxRadius = 900
    levelSettings.circles.distance = 20
    levelSettings.circles.color = {0.6,0.6,0.6}
    levelSettings.circles.alpha = 0.6
    
    levelSettings.lines.active = false

    ring(400, 20, randomColor)
    ring(500, 25, function()
        return darkGray
    end, true)
end

function level1()
    levelSettings.radius = 300

    levelSettings.star.color = {0,1,0}
    levelSettings.brightstar.color = {0.2,1,0}
    levelSettings.circles.color = {0,1,0.3}
    levelSettings.lines.color = {0.3,1,0}
    levelSettings.lines.alpha = 0.1
    levelSettings.circles.animated = false
    levelSettings.lines.animated = false
    
    levelSettings.backgroundColor = {0.1,0.05,0.0,1}
    levelSettings.clouds = { -- every cloud definition MUST have a different color per level!
    }

    -- Ring of big grey blocks, with some in a single color.
    everyOtherBrickRing(400, 20, randomColor, false)
    everyOtherBrickRing(450, 20, randomColor, false, 2, 1)
    everyOtherSolidBrickRing(500, 20, function()
        return darkGray
    end, 2, 0)
    everyOtherSolidBrickRing(650, 20, function()
        return darkGray
    end, 2, 1)
    
end

function level2()
    levelSettings.radius = 250

    -- Ring of big grey blocks, with some in a single color.
    ring(350, 16, randomColor, false)
    everyOtherBrickRing(400, 16, randomColor, false, 2, 1)
    everyOtherBrickRing(400, 16, randomColor, false, 4, 2)
    everyOtherBrickRing(450, 16, randomColor, false, 4, 2)
    everyOtherSolidBrickRing(500, 16, function()
        return darkGray
    end, 4, 0)
end

function level3()
    levelSettings.radius = 350

    -- Ring of big grey blocks, with some in a single color.
    everyOtherSolidBrickRing(250, 15, function()
        return darkGrey
    end, 5, 0)
    ring(300, 16, randomColor, false)
    everyOtherBrickRing(350, 16, randomColor, false, 2, 1)
    everyOtherBrickRing(400, 16, randomColor, false, 2, 1)
    everyOtherSolidBrickRing(450, 16, function()
        return darkGray
    end, 4, 0)
end

function level4()
    levelSettings.radius = 350

    -- Ring of big grey blocks, with some in a single color.
    everyOtherSolidBrickRing(250, 15, function()
        return darkGrey
    end, 3, 0)
    ring(300, 16, randomColor, false, 3)
    everyOtherBrickRing(350, 16, randomColor, false, 2, 1, 3)
    everyOtherBrickRing(400, 16, randomColor, false, 2, 1, 3)
    everyOtherSolidBrickRing(450, 16, function()
        return darkGray
    end, 4, 0)
end

function inner()
    levelSettings.radius = 400

    -- Ring of big grey blocks, with some in a single color.
    everyOtherSolidBrickRing(600, 40, function()
        return darkGrey
    end, 1, 0)

    everyOtherBrickRing(500, 40, function()
        return rainbowColors[4]
    end, false, 5, 1, 1)

    everyOtherBrickRing(150, 20, randomColor, false, 4, 1, 2)
    for i = 0,2 do
        everyOtherBrickRing(200, 20, randomColor, false, 4, i)
        everyOtherSolidBrickRing(250, 20, function()
            return darkGray
        end, 4, i)
    end
end

function level5()
    levelSettings.radius = 350

    levelSettings.circles.active = false
    levelSettings.circles.minRadius = 375
    levelSettings.lines.distance = 3
    levelSettings.lines.animated = true

    levelSettings.lines.color = {1,1,1}

    levelSettings.clouds = {}
    

    -- Ring of big grey blocks, with some in a single color.
    everyOtherSolidBrickRing(100, 15, function()
        return darkGrey
    end, 3, 0)

    ring(200, 16, randomColor, false )
    everyOtherBrickRing(250, 15, randomColor, false, 3, 1, 3)
    everyOtherBrickRing(450, 15, randomColor, false, 3, 1, 3)
    everyOtherBrickRing(500, 15, randomColor, false, 3, 1, 3)
    everyOtherSolidBrickRing(450, 30, function()
        return darkGray
    end, 3, 0)
    everyOtherSolidBrickRing(550, 30, function()
        return darkGray
    end, 3, 2)
end

-- level functions

function loadLevel(i)
    gameIsWon = false
    i = ((i-1) % #levels) + 1

    levelSettings = defaultLevelSettings()
    startScreen = false

    love.mouse.setRelativeMode(true)

    scene = Scene:new()
    timemachine = TimeMachine:new()

    brickCount = 0
    bricks = {}
    levels[i]()

    Particles:initParticlesFromSettings()
    scene.paddle.radius = levelSettings.radius

    colorCombo = ComboCounter:new(50, {0.5, 0.5, 0.5}) -- initialize color Combo with grey

    if currentMusic ~= nil then
        currentMusic:stop()
    end
    if currentMusicBackwards ~= nil then
        currentMusicBackwards:stop()
    end
    currentMusic = music.severe_tire_damage
    currentMusicBackwards = music.severe_tire_damage_reverse
    if slomo then
        currentMusic:setPitch(0.5)
    else
        currentMusic:setPitch(1)
    end
    currentMusicBackwards:setPitch(2)

    currentMusic:stop()
    currentMusicBackwards:stop()
    currentMusic:play()

    timeHintOn = true
    comboHintOn = true
    everComboed = false
end

function startLevelTransition()
    -- stop level music
    if currentMusic:isPlaying() then
        currentMusic:stop()
    elseif currentMusicBackwards:isPlaying() then
        currentMusicBackwards:stop()
    end
    -- play transition sound
    sounds.rocket_woosh:play()
end

function updateLevelTransition(dt)

    transitionTime = transitionTime + dt

    -- load new level at the end:
    if transitionTime >= maxTransitionTime then
        loadLevel(levelNumber)
        levelChanging = false
        transitionTime = 0
    end
end

function nextLevel()
    levelNumber = levelNumber + 1
    levelNumber = ((levelNumber-1) % #levels) + 1

    requestLevelLoad = true
end

function levelWon()
    local won = true

    for _, brick in ipairs(bricks) do
        if not brick.immune and brick:getImmunity() >= 0 then
            won = false
        end
    end

    return won
end

function levelLost()
    local anyBallsRemaining = false
    for _, ball in ipairs(scene.balls) do
        if not (ball.pos.x < 0 or ball.pos.x > CANVAS_WIDTH or ball.pos.y < 0 or ball.pos.y > CANVAS_HEIGHT) then
            anyBallsRemaining = true
        end
    end

    return not anyBallsRemaining
end

-- levels

levels = {noRiskNoFun, halvesLevel, level1, inner, level5, level4}
