
local LinearCollider = class("LinearCollider")

function LinearCollider:initialize(p1, p2)
    self.p1 = p1
    self.p2 = p2
    self.name = "LinearCollider"

    self.bounding_circle = {
        pos = (p1 + p2) / 2,
        radius = (p1 - p2):len() / 2
    }
end

function LinearCollider:draw()
    love.graphics.line(self.p1.x, self.p1.y, self.p2.x, self.p2.y)
end

function LinearCollider:getCollisionWithBall(ball, movement)
    local movedP1 = self.p1 - (self.p2 - self.p1):perpendicular():normalized() * ball.radius
    local params = self:getIntersection(ball.pos, ball.pos + movement, movedP1, movedP1 + self.p2 - self.p1)

    if params.b > 0 and params.b < 1 and params.d > 0 and params.d < 1 then
        return {
            hit = true,
            object = self,
            distanceToHit = params.b * movement:len(),
            collisionPoint = ball.pos + params.b * movement, -- center of the moving ball at the moment of collision
            normal = (self.p2 - self.p1):perpendicular(),
        }
    end

    return {
        hit = false
    }
end

function LinearCollider:getIntersection(A, B, C, D)
    -- computation of d according to https://www.matheplanet.com/matheplanet/nuke/html/viewtopic.php?topic=11311&post_id=69462
    -- computation of b by substituting variables willy-nilly.
    local b = ((D.x - C.x) * (A.y - C.y) - (D.y - C.y) * (A.x - C.x)) / ((B.x - A.x) * (D.y - C.y) - (D.x - C.x) * (B.y - A.y))
    local d = ((B.x - A.x) * (C.y - A.y) - (B.y - A.y) * (C.x - A.x)) / ((D.x - C.x) * (B.y - A.y) - (B.x - A.x) * (D.y - C.y))
    return { b = b, d = d }
end

return LinearCollider
