
local CircleCollider = class("CircleCollider")

function CircleCollider:initialize(pos, radius, angleStart, angleEnd, concave)
    self.pos = pos
    self.radius = radius
    self.name = "CircleCollider"
    self.angleStart = angleStart or 0
    self.angleEnd = angleEnd or TAU
    self.concave = concave or false

    self.bounding_circle = {
        pos = self.pos,
        radius = self.radius
    }
end

function CircleCollider:draw()
    if self.concave then
        love.graphics.setColor(0.25, 0.25, 0.75)
    else
        love.graphics.setColor(0.5, 0.5, 0.5)
    end
    love.graphics.arc("line", "open", self.pos.x, self.pos.y, self.radius, self.angleStart, self.angleEnd)
end

function CircleCollider:getCollisionWithBall(ball, movement)
    -- following https://math.stackexchange.com/a/311956
    local testRadius = 0
    if self.concave then
        testRadius = self.radius - ball.radius
    else
        testRadius = self.radius + ball.radius
    end
    local a = movement:len2()
    local dx = ball.pos.x - self.pos.x
    local dy = ball.pos.y - self.pos.y
    local b = 2 * movement.x * dx + 
              2 * movement.y * dy
    local c = dx * dx + dy * dy - testRadius * testRadius
    
    local d = b*b - 4*a*c

    if d >= 0 then 
        
        local t = -1

        if self.concave then
            t = (-b + math.sqrt(d)) / (2 * a)
        else
            t = 2 * c / (-b + math.sqrt(d))
        end

        if 0 <= t and t <= 1 then 
            local collisionPoint = ball.pos + t * movement
            local normal = (collisionPoint - self.pos):normalized()

            if self.concave then
                normal = -normal
            end

            -- check if we are on the arc
            local hitAngleOnCirlce = math.atan2((collisionPoint - self.pos).y, (collisionPoint - self.pos).x)
            if hitAngleOnCirlce < 0 then
                hitAngleOnCirlce = hitAngleOnCirlce + TAU
            end
            if hitAngleOnCirlce >= self.angleStart and hitAngleOnCirlce <= self.angleEnd then
                return {
                    hit = true,
                    distanceToHit = t * movement:len(),
                    collisionPoint = collisionPoint, -- center of the moving ball at the moment of collision
                    normal = normal,
                    object = self
                }
            end
        end
    end

    return {
        hit = false
    }
end

return CircleCollider
