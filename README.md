## Radiant Rewind

A game made in 72 hours for the [Ludum Dare](https://ldjam.com/) 47 game jam (theme: "stuck in a loop").

Using the [LÖVE](https://love2d.org) 11.3 game engine.

- Music: "Severe Tire Damage" by Kevin MacLeod
- Font: "Biysk" by Vladimir Nikolic
