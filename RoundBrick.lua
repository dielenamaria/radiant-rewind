
local RoundBrick = class("RoundBrick")

MAX_IMMUNITY = 3

BRICK_WIDTH = 30

brickCount = 0

function RoundBrick:initialize(radius, angleStart, angleEnd, color, immune, immunity)
    brickCount = brickCount + 1
    self.id = brickCount

    self.center = CENTER
    self.radius = radius
    self.radiusInner = radius - BRICK_WIDTH/2
    self.radiusOuter = radius + BRICK_WIDTH/2
    self.angleStart = angleStart
    self.angleEnd = angleEnd
    self.corner_radius = 5
    self.immune = immune
    self.explosionFrames = 0

    if self.immune then
        self:setImmunity(MAX_IMMUNITY)
    else
        self:setImmunity(immunity or 3)
    end
    
    self.color = color or {0.5, 0.5, 0.5}
    

    self.corner_inset_angle = math.atan(self.corner_radius / radius)

    self.radiusInnerInset = radius - BRICK_WIDTH/2 + self.corner_radius
    self.radiusOuterInset = radius + BRICK_WIDTH/2 - self.corner_radius
    self.angleStartInset = angleStart + self.corner_inset_angle
    self.angleEndInset = angleEnd - self.corner_inset_angle
    
    self.name = "Round Brick"

    -- init colliders
    self.colliders = {}

    self:addCornerCircle(self.angleStartInset, self.radiusInnerInset)
    self:addCornerCircle(self.angleEndInset  , self.radiusInnerInset)
    self:addCornerCircle(self.angleStartInset, self.radiusOuterInset)
    self:addCornerCircle(self.angleEndInset  , self.radiusOuterInset)

    table.insert(self.colliders, CircleCollider:new(self.center, self.radiusInner, self.angleStartInset, self.angleEndInset, true))
    table.insert(self.colliders, CircleCollider:new(self.center, self.radiusOuter, self.angleStartInset, self.angleEndInset))
    
    table.insert(self.colliders, LinearCollider:new(self:getPoint(self.angleStart, self.radiusInnerInset), self:getPoint(self.angleStart, self.radiusOuterInset)))
    table.insert(self.colliders, LinearCollider:new(self:getPoint(self.angleEnd  , self.radiusOuterInset), self:getPoint(self.angleEnd  , self.radiusInnerInset)))

    -- init polygon
    self.meshVertices = {}
    self.polyVertices = {}

    self:addCornerVertices(self.angleStartInset, self.radiusInnerInset, 0)
    for a = self.angleStartInset + 0.05, self.angleEndInset - 0.05, 0.05 do
        table.insert(self.polyVertices, math.cos(a) * self.radiusInner + self.center.x)
        table.insert(self.polyVertices, math.sin(a) * self.radiusInner + self.center.y)
    end
    self:addCornerVertices(self.angleEndInset  , self.radiusInnerInset, 1)
    
    self:addCornerVertices(self.angleEndInset  , self.radiusOuterInset, 2)
    for a = self.angleEndInset - 0.05, self.angleStartInset + 0.05, -0.05 do
        table.insert(self.polyVertices, math.cos(a) * self.radiusOuter + self.center.x)
        table.insert(self.polyVertices, math.sin(a) * self.radiusOuter + self.center.y)
    end
    self:addCornerVertices(self.angleStartInset, self.radiusOuterInset, 3)

    local triangles = love.math.triangulate(self.polyVertices)
    for _, triangle in ipairs(triangles) do
        table.insert(self.meshVertices, {triangle[1], triangle[2]})
        table.insert(self.meshVertices, {triangle[3], triangle[4]})
        table.insert(self.meshVertices, {triangle[5], triangle[6]})
    end
    self.mesh = love.graphics.newMesh(self.meshVertices, "triangles")


    -- compute overall bounding circle
    local averageAngle = (angleStart + angleEnd) / 2
    local posBrickCenter = self.center + vector(math.cos(averageAngle)*radius, math.sin(averageAngle)*radius)
    local posBrickCorner = self.center + vector(math.cos(self.angleEnd)*radius, math.sin(self.angleEnd)*radius)

    self.bounding_circle = {
        pos = posBrickCenter,
        radius = (posBrickCenter - posBrickCorner):len() + self.corner_radius
    }
end

function RoundBrick:getPoint(angle, radius)
    return self.center + vector(math.cos(angle)*radius, math.sin(angle)*radius)
end

function RoundBrick:addCornerVertices(angle, radius, i)
    local pos = self.center + vector(math.cos(angle)*radius, math.sin(angle)*radius)
    for g = 90, 0, -15 do
        local a = (180 + g - 90 * i) * TAU / 360 + angle
        table.insert(self.polyVertices, pos.x + math.cos(a) * self.corner_radius)
        table.insert(self.polyVertices, pos.y + math.sin(a) * self.corner_radius)
    end
end

function RoundBrick:addCornerCircle(angle, radius)
    table.insert(self.colliders, CircleCollider:new(self:getPoint(angle, radius), self.corner_radius))
end

function RoundBrick:draw()
    if self:getImmunity() == -1 then
        return
    end
    -- show colliders
    -- love.graphics.setColor(0.7, 0.7, 0.7)
    -- for _, circle in ipairs(self.colliders) do
    --     circle:draw()
    -- end 

    -- show actual polygon

    -- fill (color graded by immunity):
    local fillColorRed   = (1 / (MAX_IMMUNITY+1))*self:getImmunity()*self.color[1]
    local fillColorGreen = (1 / (MAX_IMMUNITY+1))*self:getImmunity()*self.color[2]
    local fillColorBlue  = (1 / (MAX_IMMUNITY+1))*self:getImmunity()*self.color[3]
    love.graphics.setColor(fillColorRed, fillColorGreen, fillColorBlue)
    love.graphics.draw(self.mesh)
    -- outline:
    love.graphics.setColor(self.color)
    love.graphics.polygon("line", self.polyVertices)

end

function RoundBrick:drawGlow()
    if self:getImmunity() == -1 and self.explosionFrames == 0 then
        return
    end
    local intensity = 0.2 + self:getImmunity() * 0.015 + self.explosionFrames * 0.25
    love.graphics.setColor((self.color[1]+0.1) * intensity, (self.color[2]+0.1) * intensity, (self.color[3]+0.1) * intensity, 1)
    local numGlows = math.max(2, (self.angleEnd - self.angleStart) / (5 / 360 * TAU))
    local s = 1 + self.explosionFrames * 0.2
    for i = 1,numGlows do
        local pos = self:getPoint(self.angleStart + (-1 + (i * 2)) / (numGlows * 2 - 1) * (self.angleEnd - self.angleStart), self.radius)
        love.graphics.draw(images.glow5, pos.x, pos.y, 0, s, s, images.glow5:getWidth()/2, images.glow5:getHeight()/2)
    end
    if self.explosionFrames > 0 then
        self.explosionFrames = self.explosionFrames - 1
    end
end

function RoundBrick:getCollisionWithBall(ball, movement)
    local minCollision = {
        hit = false,
        distanceToHit = movement:len()
    }

    for _, collider in ipairs(self.colliders) do
        local collision = collider:getCollisionWithBall(ball, movement)
        if collision.hit and collision.distanceToHit < minCollision.distanceToHit then
            minCollision = collision
        end
    end

    minCollision.object = self -- do not return the inner object

    return minCollision
end

function RoundBrick:hit(ball, collision)
    screenshake(2)

    if self:getImmunity() > 0 then
        local combo = (colorCombo.value + 1)
        sounds.brick:setPitch(1 + 0.5*combo + math.random()*0.1)
        sounds.brick:play()
    elseif self:getImmunity() == 0 then
        sounds.glass1:setPitch(0.7+math.random())
        sounds.glass1:play()
    end

    if self.immune then
        return 
    end
    
    colorCombo:setColor(self.color)
    if self.color ~= {0.5, 0.5, 0.5} then
        colorCombo:increment()
    end

    ball:setColor(self.color)

    self:setImmunity(self:getImmunity()-1)


    if self:getImmunity() >= 0 then -- block still exists
        screenshake(2)
        
        local contactPoint = collision.collisionPoint - collision.normal * ball.radius
        local angle = math.atan2(collision.normal.y, collision.normal.x)
        particles:createParticles("touch", self.color, contactPoint, 30, angle)
        particles:createParticles("dust", self.color, contactPoint, 15, 0)
    else -- block is destroyed
        self.explosionFrames = 5;
        screenshake(5)
        
        for a = self.angleStart, self.angleEnd, 8 * TAU / 360 do
            local emissionPoint = self:getPoint(a, self.radius)
            particles:createParticles("destroy", self.color, emissionPoint, 1, a + math.pi * 0.5)
            particles:createParticles("destroy", self.color, emissionPoint, 1, a - math.pi * 0.5)
        end
        particles:createParticles("dust", self.color, collision.collisionPoint, 150, 0)
    end
end

function RoundBrick:setImmunity(immunity)
    scene.brickState[self.id] = immunity
end

function RoundBrick:getImmunity()
    return scene.brickState[self.id]
end

function RoundBrick:isAlive()
    return self:getImmunity() >= 0
end

return RoundBrick
