require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"
class = require 'lib.middleclass' -- see https://github.com/kikito/middleclass

require "helpers"
TimeMachine = require "TimeMachine"
Scene = require "Scene"
Paddle = require "Paddle"
Ball = require "Ball"
RoundBrick = require "RoundBrick"
CircleCollider = require "CircleCollider"
LinearCollider = require "LinearCollider"
ComboCounter = require "ComboCounter"
Particles = require "Particles"
local moonshine = require 'lib.moonshine'

require "levels"

-- global constants
CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080

TAU = 2*math.pi

CENTER = vector(CANVAS_WIDTH/2,CANVAS_HEIGHT/2)

-- effects constants
SCREEN_SHAKE_TIMEMACHING_BEGIN = 20
SCREEN_SHAKE_TIMEMACHING_DURING = 7
GOD_RAYS_NORMAL = 0.0
GOD_RAYS_TIMEMACHINE = 0.5
GOD_RAYS_SLOMO = 0.25
GOD_RAYS_START_SCREEN = 0.3

god_rays_target = GOD_RAYS_START_SCREEN
god_rays_amount = GOD_RAYS_START_SCREEN

-- initialization for global variables

rainbowColors = {
    {1.0, 0.05, 0.3},
    {1.0, 0.4, 0.05},
    {1.0, 1.0, 0.05},
    {0.05, 1.0, 0.3},
    {0.05, 0.3, 1.0},
    {0.4, 0.05, 1.0}
}

lightGray = {0.5, 0.5, 0.5}
darkGray = {0.2, 0.2, 0.2}

bricks = {}
scene = nil
timemachine = nil
colorCombo = nil
paused = false
timetravelling = false
effectTime = 2340
slomo = false
slomoSpeed = 1 -- in percent of normal speed
minSlomoSpeed = 0.2
slomoChange = 0.02
slomoFrame = 0 -- we count the frames to discard some of them in the time machine...

currentMusic = nil
currentMusicBackwards = nil

levelNumber = 1
requestLevelLoad = false
levelChanging = false
transitionTime = 0
maxTransitionTime = 1.45 -- duration of the sound effect (rocket_woosh)

screenshakeAmount = 0
time_since_cloud = 0

startScreen = true
hintOpacity = 0
timeHintOn = false
comboHintOn = false
everRewinded = false
everSlomoed = false
everComboed = false

levelSettings = {}
gameInWon = false

function love.load()
    -- set up default drawing options
    love.graphics.setBackgroundColor(0, 0, 0)

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end
    sounds.zoom_up:setVolume(0.3)
    sounds.blub:setVolume(0.9)
    sounds.blub_reverse:setVolume(1.0)
    sounds.plop:setVolume(0.4)
    sounds.brick:setVolume(0.4)
    sounds.glass1:setVolume(0.3)
    sounds.rocket_woosh:setVolume(0.3)

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
            music[filename:sub(1,-5)]:setVolume(0.25)
        end
    end

    currentMusic = music.severe_tire_damage
    currentMusicBackwards = music.severe_tire_damage_reverse

    fonts = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        if filename ~= ".gitkeep" then
            fonts[filename:sub(1,-5)] = {}
            fonts[filename:sub(1,-5)][20] = love.graphics.newFont("fonts/"..filename, 20)
            fonts[filename:sub(1,-5)][50] = love.graphics.newFont("fonts/"..filename, 50)
            fonts[filename:sub(1,-5)][75] = love.graphics.newFont("fonts/"..filename, 75)
            fonts[filename:sub(1,-5)][100] = love.graphics.newFont("fonts/"..filename, 100)
            fonts[filename:sub(1,-5)][150] = love.graphics.newFont("fonts/"..filename, 150)
            fonts[filename:sub(1,-5)][200] = love.graphics.newFont("fonts/"..filename, 200)
            fonts[filename:sub(1,-5)][250] = love.graphics.newFont("fonts/"..filename, 250)
            fonts[filename:sub(1,-5)][300] = love.graphics.newFont("fonts/"..filename, 300)
        end
    end
    mainFont = fonts.biysk
    love.graphics.setFont(mainFont[50])

    love.mouse.setRelativeMode(true)

    -- setup effects

    --effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.colorgradesimple).chain(moonshine.effects.crt).chain(moonshine.effects.glow)
    effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.colorgradesimple).chain(moonshine.effects.glow).chain(moonshine.effects.godsray)
    effect.godsray.exposure = GOD_RAYS_NORMAL
    effect.godsray.weight = GOD_RAYS_NORMAL
    effect.godsray.samples = 10
    effect.godsray.density = 0.10
    effect.glow.min_luma = 0.7
    effect.glow.strength = 2
    effect.scanlines.width = 2
    effect.scanlines.thickness = 1
    effect.scanlines.opacity = 0.15
    --effect.colorgradesimple.factors = {2,2,2}
    --effect.chromasep.angle = 0
    --effect.chromasep.radius = 5

    levelSettings = defaultLevelSettings()
    startScreenLevelSettings()

    -- comment in the following lines to disable the effects: --
    -- effect.disable("scanlines")
    effect.disable("colorgradesimple")
    effect.disable("glow")
    effect.disable("godsray")

    Particles:initParticlesFromSettings()
end

function defaultLevelSettings() 
    return {
        name = "No Name",
        backgroundColor = {0.05, 0.0, 0.05},
        star = {
            number = 30,
            color = {1,1,1,1}
        },
        brightstar = {
            number = 30,
            color = {1,1,1,1}
        },
        radius = 350,
        clouds = { -- every cloud definition MUST have a different color per level!
            {
                color = {0.20,0.03,0.14},
                pos = vector(600,600)
            },
            {
                color = {0.07,0.03,0.10},
                pos = vector(1700,200)
            },
            {
                color = {0.02,0.05,0.12},
                pos = vector(200,100)
            }
        },
        circles = {
            color = {0.8, 0.2, 1.00},
            active = true,
            distance = 50,
            animated = true,
            alpha = 0.25,
            minRadius = 500,
            maxRadius = 2300,
        },
        lines = {
            color = {0.8, 0.2, 1.00},
            active = true,
            distance = 5,
            animated = true,
            alpha = 0.2
        }
    }
end

function startScreenLevelSettings()
    levelSettings = defaultLevelSettings()
    levelSettings.clouds = {}
    for _, col in ipairs(rainbowColors) do
        local f = 0.15
        table.insert( levelSettings.clouds, 
            {
                color = {col[1] * f, col[2] * f, col[3] * f},
                pos = vector(math.random(0,1920),math.random(0,1080))
            }
        )
    end
end

function love.update(dt)
    dt = dt*slomoSpeed
    
    time_since_cloud = time_since_cloud + dt
    effectTime = effectTime + dt
    if time_since_cloud > 1 then
        for _, cloud in ipairs(levelSettings.clouds) do
            particles:createParticles("cloud", cloud.color, cloud.pos, 1, 0)
        end
        time_since_cloud = 0
    end

    if startScreen or not paused then
        particles:update(dt)
    end

    if not paused then
        slomoFrame = slomoFrame + 1

        if ((timeHintOn and levelNumber == 1) or (comboHintOn and levelNumber == 2)) and hintOpacity < 1 then
            hintOpacity = hintOpacity + 0.01
        end
        if (not timeHintOn and levelNumber == 1) or (not comboHintOn and levelNumber == 2) and hintOpacity > 0 then
            hintOpacity = hintOpacity - 0.01
        end

        if everRewinded and everSlomoed then
            timeHintOn = false
        end

        if everComboed then
            comboHintOn = false
        end

        if slomo then
            if slomoSpeed > minSlomoSpeed then
                slomoSpeed = slomoSpeed - slomoChange
            end
        else
            if slomoSpeed < 1 then
                slomoSpeed = slomoSpeed + slomoChange
            end
        end

        

        if levelWon() and not requestLevelLoad and not levelChanging and not startScreen then
            if levelNumber < #levels then
                nextLevel()
            else
                gameIsWon = true
            end
        end

        if requestLevelLoad then
            levelChanging = true
            startLevelTransition()
            requestLevelLoad = false
        end

        if levelChanging then
            updateLevelTransition(dt)
        end

        if not startScreen then
            -- Reduce screenshake.
            if screenshakeAmount > 0 then
                screenshakeAmount = math.max(0, screenshakeAmount - 25 * dt)
            end

            if timetravelling and timemachine:hasPast() then
                effect.godsray.light_x = 0.5 + math.sin(effectTime*10)/4
                effect.godsray.light_y = 0.5 + math.cos(effectTime*10)/4
            else
                effect.godsray.light_x = 0.5
                effect.godsray.light_y = 0.5
            end

            if timetravelling then
                scene = timemachine:pop() or scene
                scene = timemachine:pop() or scene

                if screenshakeAmount < SCREEN_SHAKE_TIMEMACHING_DURING then
                    screenshakeAmount = SCREEN_SHAKE_TIMEMACHING_DURING
                end

                if not timemachine:hasPast() and currentMusicBackwards:isPlaying() then
                    currentMusicBackwards:pause()
                end
            else
                scene:update(dt)

                if slomo then
                    if slomoFrame % (1/minSlomoSpeed) == 0 then
                        timemachine:push(scene)
                    end
                else
                    timemachine:push(scene)       
                end
            end
            --combo:update(dt)
            colorCombo:update(dt)
        end
    end


    if god_rays_amount < god_rays_target then
        god_rays_amount = math.min(god_rays_target, god_rays_amount + dt)
    end
    if god_rays_amount > god_rays_target then
        god_rays_amount = math.max(god_rays_target, god_rays_amount - dt)
    end

    if god_rays_amount <= 0 then
        effect.disable("godsray")
    else
        effect.enable("godsray")
    end
    effect.godsray.exposure = god_rays_amount
    effect.godsray.weight = god_rays_amount
end

function love.mouse.getPosition()
    return vector(tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT))
end

function love.mousepressed(x, y, button, istouch, presses)
    if button == 1 and startScreen and not paused then
        requestLevelLoad = true
        god_rays_target = GOD_RAYS_NORMAL
    end
end

function love.keypressed(key)
    if key == "escape" then
        love.window.setFullscreen(false)
        love.event.quit()
    elseif key == "f" then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    end
    
    if startScreen then
        -- put key bindings for start screen here:
        if key == "p" then
            love.mouse.setRelativeMode(paused)
            paused = not paused
        elseif key == "space" then
            requestLevelLoad = true
        end
    else
        if levelChanging then
            return
        end
        -- put key bindings for playing screen here:
        if key == "space" and not paused and not startScreen then
            rewindOn()
        elseif (key == "lctrl" or key == "rctrl") and not paused and not slomo then
            slomoOn()
        elseif key == "p" then
            love.mouse.setRelativeMode(paused)
            paused = not paused
            if paused then
                if timetravelling then
                    currentMusicBackwards:pause()
                else 
                    currentMusic:pause()
                end
            else
                if timetravelling then
                    local pos = currentMusicBackwards:tell()
                    currentMusicBackwards:play()
                    currentMusicBackwards:seek(pos)
                else
                    local pos = currentMusic:tell()
                    currentMusic:play()
                    currentMusic:seek(pos)
                end
            end
        -- cheats: --

        -- elseif key == "a" then
            --scene:addBall()
        -- elseif key == "q" then
            --scene:resetToOneBall()
            
        elseif key == "r" and not timetravelling then
            requestLevelLoad = true
        elseif key == "c" then
            colorCombo:triggerEffect()
        elseif string.match(key, "^[0-9]$") and not timetravelling then
            local k = tonumber(key)
            if k > 0 and k <= #levels then
                levelNumber = k
                requestLevelLoad = true
            end
        end
    end
end

function love.keyreleased(key)
    if key == "space" and not paused and not startScreen then
        rewindOff()
    elseif (key == "lctrl" or key == "rctrl") and not paused and slomo and not startScreen then
        slomoOff()
    end
end

function rewindOn()
    if levelChanging then
        return
    end
    if slomo then
        slomoOff()
    end
    everRewinded = true
    timetravelling = true
    if currentMusic ~= nil and currentMusic:isPlaying() then
        local musicPosition = math.max(0, currentMusic:getDuration() - (currentMusic:tell() or 0))
        currentMusic:pause()
        currentMusicBackwards:setPitch(2)
        currentMusicBackwards:play()
        currentMusicBackwards:seek(musicPosition)
    end
    screenshakeAmount = SCREEN_SHAKE_TIMEMACHING_BEGIN
    colorCombo:reset()
    god_rays_target = GOD_RAYS_TIMEMACHINE
end

function rewindOff()
    if levelChanging then
        return
    end
    timetravelling = false
    if currentMusicBackwards ~= nil and currentMusicBackwards:isPlaying() then
        local musicPosition = math.max(0, currentMusicBackwards:getDuration() - (currentMusicBackwards:tell() or 0)/2)
        currentMusicBackwards:pause()
        currentMusic:seek(musicPosition)
    end
    currentMusic:play()
    god_rays_target = GOD_RAYS_NORMAL

    if love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl") then
        slomoOn()
    end
end

function slomoOn()
    if timetravelling or levelChanging then
        return
    end

    everSlomoed = true
    sounds.blub_reverse:play()
    slomo = true
    god_rays_target = GOD_RAYS_SLOMO

    if currentMusic:isPlaying() then
        local pos = currentMusic:tell() or 0
        currentMusic:stop()
        currentMusic:setPitch(0.5)
        currentMusic:play()
        currentMusic:seek(pos)
    end
end

function slomoOff()
    sounds.blub:play()
    slomo = false
    god_rays_target = GOD_RAYS_NORMAL

    if currentMusic:isPlaying() then
        local pos = currentMusic:tell() or 0
        currentMusic:stop()
        currentMusic:setPitch(1)
        currentMusic:play()
        currentMusic:seek(pos)
    end
end

function love.mousemoved(x, y, dx, dy, istouch)
    if not startScreen then
        local clockwise = vector(0, 1):rotated(scene.paddle.angle)
        scene.clockwise_amount = scene.clockwise_amount + (vector(dx, dy)*clockwise) / clockwise:len()
    end
end

function love.draw()
    love.graphics.setBackgroundColor(levelSettings.backgroundColor)

    effect(function()
        tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

        -- Shake screen!
        if not paused then
            love.graphics.translate((math.random()-0.5)*screenshakeAmount, (math.random()-0.5)*screenshakeAmount)
        end

        love.graphics.setBlendMode("add")
        particles:draw(true)
        drawBackgroundLines()
        love.graphics.setBlendMode("alpha")

        if startScreen then
            drawStartScreen()
        else
            scene:draw()
            timemachine:draw()
            --combo:draw()
            colorCombo:draw()

            -- draw particles
            love.graphics.setBlendMode("add")
            love.graphics.setColor(1,1,1,1)
            particles:draw(false)
            love.graphics.setBlendMode("alpha")

            -- display the current frame rate: --
            --love.graphics.setColor(0.5,0.5, 0.5)
            --love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 20, 20)

            if gameIsWon then
                drawGameWon()
            elseif levelLost() then
                drawRestartHint()
            elseif levelNumber == 1 and not paused then
                drawTimeHint()
            elseif levelNumber == 2 and not paused then
                drawComboHint()
            end
        end

        if paused then
            drawPause()
        end

        -- fade to black on level transitions:
        local alpha = 1 - (maxTransitionTime - transitionTime)/maxTransitionTime
        love.graphics.setColor(0, 0, 0, alpha)
        love.graphics.rectangle("fill", 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)

        tlfres.endRendering()
    end)
end

function love.resize(w, h)
    effect.resize(w, h)
end

function screenshake(amount)
    screenshakeAmount = amount
end

function colorCycle(i)
    local speed = 0.05

    return {
        0.5+0.5*math.cos(i*speed + 0),
        0.5+0.5*math.cos(i*speed + TAU/3),
        0.5+0.5*math.cos(i*speed + TAU*2/3),
    }
end

function drawStartScreen()
    love.graphics.setColor(colorCycle(slomoFrame))
    love.graphics.setFont(mainFont[250])
    local scale = 1 + 0.02*math.sin(0.03*slomoFrame)
    love.graphics.printf("Radiant Rewind", CANVAS_WIDTH/2, 150, CANVAS_WIDTH, "center", 0, scale, scale, CANVAS_WIDTH/2)

    love.graphics.setColor(0.7, 0.7, 0.7)

    love.graphics.setFont(mainFont[75])
    love.graphics.printf("Made in 72 hours for Ludum Dare 47\nby blinry, flauschzelle, larkinia, and lenaschimmel", 0, 450, CANVAS_WIDTH, "center")

    love.graphics.setColor(1.0, 1.0, 1.0)

    --love.graphics.printf("hold left control for slow motion\n", 0, 600, CANVAS_WIDTH, "center")

    love.graphics.setFont(mainFont[75])
    love.graphics.printf("Click to start!", 0, 840, CANVAS_WIDTH, "center")
end

function drawGameWon()
    love.graphics.setColor(1.0, 1.0, 1.0)
    love.graphics.printf("Thanks for playing!\nYou can go back to a level\nby pressing the number keys.", CANVAS_WIDTH/2, CANVAS_HEIGHT/2, CANVAS_WIDTH, "center", 0, 1, 1, CANVAS_WIDTH/2, 80)
end

function drawRestartHint()
    love.graphics.setColor(1.0, 1.0, 1.0)
    love.graphics.printf("hold space to rewind\npress r to reload level", CANVAS_WIDTH/2, CANVAS_HEIGHT/2, CANVAS_WIDTH, "center", 0, 1, 1, CANVAS_WIDTH/2, 50)
end

function drawTimeHint()
    love.graphics.setColor(1.0, 1.0, 1.0, hintOpacity)
    love.graphics.printf("hold space to rewind\nhold ctrl to slow down time", CANVAS_WIDTH/2, CANVAS_HEIGHT/2, CANVAS_WIDTH, "center", 0, 1, 1, CANVAS_WIDTH/2, 60)
end

function drawComboHint()
    love.graphics.setColor(1.0, 1.0, 1.0, hintOpacity)
    love.graphics.printf("hit the same color for combos\nto unlock powerups \n\n rewinding will break your combo", CANVAS_WIDTH/2, CANVAS_HEIGHT/2, CANVAS_WIDTH, "center", 0, 1, 1, CANVAS_WIDTH/2, 80)
end

function drawPause()
    love.graphics.setColor(1.0, 1.0, 1.0)
    love.graphics.printf("-- PAUSED --", CENTER.x-100, CENTER.y-50, 200, "center")
end

function drawBackgroundLines() 
    love.graphics.setLineWidth(2)
    for r = levelSettings.circles.minRadius,levelSettings.circles.maxRadius,levelSettings.circles.distance do
        local t = (r - levelSettings.circles.minRadius) / (levelSettings.circles.maxRadius - levelSettings.circles.minRadius)
        local a = levelSettings.circles.alpha
        if levelSettings.circles.animated then
            a = -0.1 + math.sin(effectTime * (2+t) * 0.3) * levelSettings.circles.alpha
        end
        --local a_s = ((effectTime * (17.4 + t)) % 360) / 360 * TAU
        --local a_e = ((effectTime * (13.23 + t)) % 360) / 360 * TAU + TAU
        if levelSettings.circles.active then
            local c = levelSettings.circles.color
            love.graphics.setColor(c[1], c[2], c[3], a)
            -- love.graphics.arc("line", "open", CENTER.x, CENTER.y, r, a_s, a_e)
            love.graphics.circle("line", CENTER.x, CENTER.y, r)
        end
        if levelSettings.lines.active then
            a = levelSettings.lines.alpha
            if levelSettings.lines.animated then
                a = -0.1 + math.sin(effectTime * (2+t) * 0.3) * 0.25
            end
            local c = levelSettings.lines.color
            love.graphics.setColor(c[1], c[2], c[3], a)
            for deg = 0, 359, levelSettings.lines.distance do
                local ang = deg / 360 * TAU
                love.graphics.line(CENTER.x + math.sin(ang) * (r - 25), CENTER.y + math.cos(ang) * (r - 25), CENTER.x + math.sin(ang) * (r + 25), CENTER.y + math.cos(ang) * (r + 25))
            end
        end
    end

    -- for a = 0, 359, 5 do
    --     local t = a / 360
    --     local alp = -0.1 + math.sin(effectTime * (2+t) * 0.3) * 0.25
    --     love.graphics.setColor(0.8, 0.2, 1.00, alp)
    --     love.graphics.line(CENTER.x + math.sin(a) * 500, CENTER.y + math.cos(a) * 500, CENTER.x + math.sin(a) * 2300, CENTER.y + math.cos(a) * 2300)
    -- end

    love.graphics.setLineWidth(2)
end
