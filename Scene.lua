local Scene = class("Scene")

function Scene:initialize()
    self.paddle = Paddle:new()
    -- start with one ball:
    self.balls = {}
    table.insert(self.balls, Ball:new())

    self.clockwise_amount = 0
    self.brickState = {}
    math.randomseed(os.time())
end

function Scene:addBall()
    sounds.zoom_up:setPitch((0.7+math.random()))
    sounds.zoom_up:play()
    table.insert(self.balls, Ball:new())
end

function Scene:resetToOneBall()
    ball = self.balls[1]
    self.balls = {}
    table.insert(self.balls, ball)
end

function Scene:update(dt)
    local max_clockwise_amount = dt * 12
    self.paddle:update(dt)

    self.paddle:setAngle(scene.paddle.angle + clampAbs(self.clockwise_amount/200, max_clockwise_amount))
    self.clockwise_amount = 0
    for _, ball in ipairs(self.balls) do
        ball:update(dt, self:allObjects())
    end
    
end

function Scene:allObjects()
    local objects = {self.paddle}

    for _, ball in ipairs(self.balls) do
        table.insert(objects, ball)
    end

    for _, brick in ipairs(bricks) do
        table.insert(objects, brick)
    end

    return objects
end

function Scene:draw()
    self.paddle:draw()

    for _, ball in ipairs(self.balls) do
        ball:draw()
    end

    for _, brick in ipairs(bricks) do
        brick:draw()
    end

    love.graphics.setBlendMode("add")
    for _, brick in ipairs(bricks) do
        brick:drawGlow()
    end
    for _, ball in ipairs(self.balls) do
        ball:drawGlow()
    end
    self.paddle:drawGlow()
    love.graphics.setBlendMode("alpha")
end

return Scene
