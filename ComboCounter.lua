local ComboCounter = class("ComboCounter")

function ComboCounter:initialize(posY, color)
    self.value = 0
    self.scale = 1
    local pY = posY or 50
    self.pos = vector(CANVAS_WIDTH - 50, posY)
    self.color = color or {1.0, 1.0, 1.0}
    -- for highscore:
    self.highscore = 0
    self.highscoreColor = self.color
    -- for effects:
    self.effectUsed = false
    self.effectName = "multiball"
end

function ComboCounter:increment()
    if self.scale < 1.2 then
        self.scale = 1.2
    end
    self.value = self.value + 1

    if self.value == 2 then
        everComboed = true
    end

    -- highscore:
    if self.value > self.highscore then
        self.highscore = self.value
        self.highscoreColor = self.color
    end
end

function ComboCounter:reset()
    self.scale = 2.5
    self.value = 0
    self.effectUsed = false
end

function ComboCounter:update(dt)
    if self.scale > 1 then
        local scaleDiff = self.scale - 1
        self.scale = self.scale - scaleDiff*2*dt
    end
end

function ComboCounter:setColor(color)
    if self.color ~= color then
        self.color = color
        self:reset()
    end
end

function ComboCounter:draw() 
    -- draw current combo:
    love.graphics.setColor(self.color)
    love.graphics.setFont(mainFont[100])
    love.graphics.printf("Combo: " ..self.value, CANVAS_WIDTH-30, 30, CANVAS_WIDTH-60, "right", 0, self.scale/2, self.scale/2, CANVAS_WIDTH-60, 0)

    -- draw effect info:
    if self:effectAvailable() then
        self:drawEffectInfo()
    end

    -- draw highscore:
    love.graphics.setColor(self.highscoreColor)
    love.graphics.setFont(mainFont[50])
    love.graphics.printf("Max Combo: " ..self.highscore, CANVAS_WIDTH-30, 130, CANVAS_WIDTH-60, "right", 0, 1, 1, CANVAS_WIDTH-60, 0)
end

function ComboCounter:triggerEffect()
    if self:effectAvailable() then
        self.effectUsed = true
        if self.effectName == "multiball" then
            -- multiball effect:
            for i = 1,(self.value -1) do
                scene:addBall()
            end
        elseif self.effectName == "big ball" then
            -- big ball effect:
            for _, ball in ipairs(scene.balls) do
                ball.radius = ball.radius + (10 * (self.value -1))
            end
        end
        self:reset()
    end
end

function ComboCounter:drawEffectInfo()

    love.graphics.setFont(mainFont[50])
    love.graphics.printf("press C for\n powerup:", CANVAS_WIDTH-30, 280, CANVAS_WIDTH-60, "right", 0, 1, 1, CANVAS_WIDTH-60, 0)
   
    love.graphics.setFont(mainFont[100])
    love.graphics.printf(self.effectName, CANVAS_WIDTH-30, 430, CANVAS_WIDTH-60, "right", 0, 1, 1, CANVAS_WIDTH-60, 0)
end

function ComboCounter:effectAvailable()
    if self.value >= 2 and self.effectUsed == false then
        if self.color[1] > 0.5 then
            -- multiball for red-ish colors
            self.effectName = "multiball"
        else
            -- big ball for cyan-ish colors
            self.effectName = "big ball"
        end
        return true
    else
        return false
    end
end

return ComboCounter
