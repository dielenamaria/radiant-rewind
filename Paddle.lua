
local Paddle = class("Paddle")


function Paddle:initialize()
    self.radius = levelSettings.radius
    self.angle = 0
    self.name = "Paddle"
    self.movement = vector(0,0)
    self.extraGlow = 0
    self.extraGlowColor = {1,1,1,1}

    self.bounding_circle = {
        pos = CENTER,
        radius = 50
    }
end

function Paddle:setAngle(angle)
    self.angle = angle
    local oldPos = self.bounding_circle.pos
    local offset = vector(math.cos(self.angle)*self.radius, math.sin(self.angle)*self.radius)
    self.pos = CENTER + offset
    self.bounding_circle.pos = CENTER + offset
    self.movement = self.bounding_circle.pos - oldPos
end

function Paddle:draw()
    -- Draw paddle's circle.
    love.graphics.setColor(levelSettings.circles.color)
    love.graphics.circle("line", CENTER.x, CENTER.y, self.radius)

    love.graphics.setColor(1,1,1)
    love.graphics.draw(images.raccoon, self.pos.x, self.pos.y, self.angle+math.pi/2,1,1,images.raccoon:getWidth()/2,images.raccoon:getHeight()/2)
    love.graphics.circle("line",self.bounding_circle.pos.x,self.bounding_circle.pos.y,self.bounding_circle.radius)
end


function Paddle:drawGlow()
    local g = 0.45
    love.graphics.setColor(g,g,g*1.5,1)
    love.graphics.draw(images.racoon_glow, self.pos.x, self.pos.y, self.angle+math.pi/2,1,1,images.racoon_glow:getWidth()/2,images.racoon_glow:getHeight()/2)
    local g = 0.15
    love.graphics.setColor(g,g,g*1.5,1)
    love.graphics.draw(images.ring_glow, self.pos.x, self.pos.y, 0,1,1,images.ring_glow:getWidth()/2,images.ring_glow:getHeight()/2)
    
    if self.extraGlow > 0 then
        g = self.extraGlow * 1
        love.graphics.setColor(self.extraGlowColor[1] * g, self.extraGlowColor[2] * g, self.extraGlowColor[3] * g, 1)
        love.graphics.draw(images.ring_glow, self.pos.x, self.pos.y, 0,1,1,images.ring_glow:getWidth()/2,images.ring_glow:getHeight()/2)
        g = self.extraGlow * 0.25
        love.graphics.setColor(self.extraGlowColor[1] * g, self.extraGlowColor[2] * g, self.extraGlowColor[3] * g, 1)
        love.graphics.draw(images.glow5, self.pos.x, self.pos.y, 0,0.3 + self.extraGlow / 2,0.3 + self.extraGlow / 2,images.glow5:getWidth()/2,images.glow5:getHeight()/2)
    end
end

function Paddle:update(dt)
    if self.extraGlow > 0 then
        self.extraGlow = math.max(0, self.extraGlow - dt * 10)
    end
end


function Paddle:getCollisionWithBall(ball, movement)
    movement = movement - self.movement -- relative movement between ball and paddle
    -- following https://math.stackexchange.com/a/311956
    local testRadius = self.bounding_circle.radius + ball.radius
    local a = movement:len2()
    local dx = ball.pos.x - self.bounding_circle.pos.x
    local dy = ball.pos.y - self.bounding_circle.pos.y
    local b = 2 * movement.x * dx + 
              2 * movement.y * dy
    local c = dx * dx + dy * dy - testRadius * testRadius
    
    local d = b*b - 4*a*c

    if d >= 0 then 
        local t = 2 * c / (-b + math.sqrt(d))
        if -4 <= t and t <= 1 then 
            local collisionPoint = ball.pos + t * movement
            local normal = (collisionPoint - self.bounding_circle.pos):normalized()

            return {
                hit = true,
                distanceToHit = t * movement:len(),
                collisionPoint = collisionPoint, -- center of the moving ball at the moment of collision
                normal = normal,
                object = self
            }
        end
    end

    return {
        hit = false
    }
end

function Paddle:hit(ball)
    sounds.plop:setPitch(0.5+math.random())
    sounds.plop:play()
    self.extraGlow = 3
    self.extraGlowColor = ball.color
end

function Paddle:isAlive()
    return true
end

return Paddle
